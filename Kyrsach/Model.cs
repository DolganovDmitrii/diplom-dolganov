﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Kyrsach
{
    
    //алгоритм Марсальи
    class NormalRandom : Random
    {
       
        // сохранённое предыдущее значение
        double prevSample = double.NaN;
        protected override double Sample()
        {
            // есть предыдущее значение? возвращаем его
            if (!double.IsNaN(prevSample))
            {
                double result = prevSample;
                prevSample = double.NaN;
                return result;
            }

            // нет? вычисляем следующие два
            // Marsaglia polar method из википедии
            double u, v, s;
            do
            {
                u = 2 * base.Sample() - 1;
                v = 2 * base.Sample() - 1; // [-1, 1)
                s = u * u + v * v;
            }
            while (u <= -1 || v <= -1 || s >= 1 || s == 0);
            double r = Math.Sqrt(-2 * Math.Log(s) / s);

            prevSample = r * v;
            return r * u;
        }
    }

    class Model
    {
        public const double PI = 3.14159265358979;
        
        
        public double amax;
        public double amin;
        public double pmax;
        public double pmin;
        public double[] Z; //ДН
        public double[] Z1; //ДН
        public double[] Z_tt;
        public double[] A; //Азимут
        public double[] P; //Место
        public double[] T; //Параметры
        public double[] R1; //Рассчётные
        public double[] R11; //Рассчётные
        public double[] R2; //Рассчётные
        public double[] R22; //Рассчётные
        public double[] SA; //Сигнал A
        public double[] SP; //Сигнал P
        public double[] kef; //коэффициенты
        public double[] osh;// error
        public double[] fi; //ДН для функционала
        
        public int realasim = 0;
        public int realplace = 0;
        
        public Model()
        {
            
            A = new double[8];
            P = new double[8];
            T = new double[2];
            R1 = new double[200];
            R11 = new double[200];
            R2 = new double[200];
            R22 = new double[200];
            Z = new double[R1.Length * R2.Length];
            Z1 = new double[R1.Length * R2.Length];
            Z_tt = new double[R1.Length * R2.Length];
            SA = new double[1];
            SP = new double[1];
            kef = new double[6];
            osh = new double[1];
            fi = new double[R1.Length * R2.Length];
            

        }
        private void DoubleArray(double[] X, int n, string str)
        {
            string[] s = str.Split();
            for (int i = 0; i < n; i++)
            {
                X[i] = Convert.ToDouble(s[i]);
            }
        }

        internal void SetA(TextBox t1, TextBox t2, TextBox t3,TextBox t4,TextBox t5,TextBox t6, TextBox t7, TextBox t8)
        {
            DoubleArray(A, 8, t1.Text + " " + t2.Text + " " + t3.Text + " " + t4.Text + " " + t5.Text + " " + t6.Text + " " + t7.Text + " " + t8.Text);
        }
        internal void SetP(TextBox t1, TextBox t2, TextBox t3, TextBox t4, TextBox t5, TextBox t6, TextBox t7, TextBox t8)
        {
            DoubleArray(P, 8, t1.Text + " " + t2.Text + " " + t3.Text + " " + t4.Text + " " + t5.Text + " " + t6.Text + " " + t7.Text + " " + t8.Text);
        }
        internal void SetT(TextBox t1, TextBox t2)
        {
            DoubleArray(T, 2, t1.Text + " " + t2.Text);
        }
        internal void SetSA(TextBox t1)
        {
            DoubleArray(SA, 1, t1.Text);
        }
        internal void SetSP(TextBox t1)
        {
            DoubleArray(SP, 1, t1.Text);
        }
        internal void SetER(TextBox t1)
        {
            DoubleArray(osh, 1, t1.Text);
        }
        
        internal void Signal()
        {

            double l = 0.1;
            for (int i = 0; i < R1.Length; i++)
            {
                R1[i] = A[7] + l;
                R11[i] = A[7] + l;
                l = l + 0.1;
                if (Math.Round(SA[0], 2) == Math.Round(R1[i], 2)) realasim = i;
            }
            l = 0.1;
            for (int i = 0; i < R2.Length; i++)
            {
                R2[i] = P[7] + l;
                R22[i] = P[7] + l;
                l = l + 0.1;
                if (Math.Round(SP[0], 2) == Math.Round(R2[i], 2)) realplace = i;
            }
            l = 0;
            amin = A[7];
            amax = A[6];
            pmin = P[7];
            pmax = P[6];
            for (int k = 0; k < 6; k++)
            {
                for (int i = 0; i < R1.Length; i++)
                {
                    for (int j = 0; j < R2.Length; j++)
                    {
                        Z_tt[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[k]) * (R1[i] - P[k]) / (2 * T[0] * T[0])) + ((R2[j] - A[k]) * (R2[j] - A[k]) / (2 * T[1] * T[1]))));

                        if (Z[i * R2.Length + j] < Z_tt[i * R2.Length + j])
                        {
                            Z[i * R2.Length + j] = Z_tt[i * R2.Length + j];
                            Z1[i * R2.Length + j] = Z_tt[i * R2.Length + j];
                        }

                        double X; double Y; double N; double deviation; double expectation;
                        var rand = new Random();

                        if (i == realplace)
                        {
                            if (j == realasim)
                            {
                                kef[k] = Z_tt[i * R2.Length + j];

                                //расчёт дисперсии 

                                deviation = (1 / Math.Sqrt(2)) * Math.Pow(10, -(osh[0] / 20));
                                expectation = Math.Sqrt(deviation);

                                NormalRandom nr = new NormalRandom();
                                X = nr.NextDouble() * deviation + expectation;
                                Y = nr.NextDouble() * deviation + expectation;
                                N = Math.Sqrt(X * X + Y * Y);
                                //Если нужно получить нормальное распределение с другими μ и σ, применяйте скалирование:
                                //double x = nr.NextDouble() * deviation + expectation;

                                kef[k] = kef[k] + N;

                            }
                        }

                    }
                }
            }


        }


        //Функционал

        internal void Functional()
        {
            for (int i = 0; i < R1.Length; i++)
            {
                for (int j = 0; j < R2.Length; j++)
                {
                    //правильное fi[i * R2.Length + j] = Math.Exp(-(((R1[i] - coor[1]) * (R1[i] - coor[1]) / (2 * T[0] * T[0])) + ((R2[j] - coor[0]) * (R2[j] - coor[0]) / (2 * T[1] * T[1]))));
                    //fi[i * R2.Length + j] = Math.Exp(-(((R1[i] - 0) * (R1[i] - 0) / (2 * T[0] * T[0])) + ((R2[j] - 0) * (R2[j] - 0) / (2 * T[1] * T[1]))));//временное
                   // if (Z1[i * R2.Length + j] < fi[i * R2.Length + j])
                    //{
                    //    Z1[i * R2.Length + j] = fi[i * R2.Length + j];
                    //}
                }
            }

            for(int k = 0; k<6; k++)
            {
                for(int l =0; l<6; l++)
                {

                }
            }


        }
        internal double[] GetX()
        {
            //write(test);
            return R1;
        }
        internal double[] GetY()
        {
            return R2;
        }
        internal double[] GetZ()
        {
            return Z;
            //return Z_tt;
        }
        internal double[] GetX1()
        {
            //write(test);
            return R11;
        }
        internal double[] GetY1()
        {
            return R22;
        }
        internal double[] GetZ1()
        {
            return Z1;
        }
        internal double[] GetSA()
        {
            return SA;
        }
        internal double[] GetSP()
        {
            return SP;
        }
        internal double getAmax()
        {
            return A[6];
        }
        internal double getPmax()
        {
            return P[6];
        }
        internal double getAmin()
        {
            return A[7];
        }
        internal double getPmin()
        {
            return P[7];
        }
        internal double getAmax1()
        {
            return amax;
        }
        internal double getPmax1()
        {
            return pmax;
        }
        internal double getAmin1()
        {
            return amin;
        }
        internal double getPmin1()
        {
            return pmin;
        }
        internal double getkef1()
        {
            return kef[0];
            //return releyerr[0];
            //return testo;
            
        }
        internal double getkef2()
        {
            return kef[1];
            //return releyerr[1];
        }
        internal double getkef3()
        {
            return kef[2];
            //return releyerr[2];
        }
        internal double getkef4()
        {
            return kef[3];
            //return releyerr[3];
        }
        internal double getkef5()
        {
            return kef[4];
            //return releyerr[4];
        }
        internal double getkef6()
        {
            return kef[5];
            //return releyerr[5];
        }

        /*private void write(double[] X)
        {
            System.IO.StreamWriter textFile = new System.IO.StreamWriter(@"D:\Diplom\Kyrsach\test.txt");
            for (int i = 0; i < X.Length; i++)
            {
                
                textFile.WriteLine(X[i]);
            }
            textFile.Close();
        }*/   

    }
}

