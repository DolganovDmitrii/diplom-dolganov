﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kyrsach
{

    class Functional
    {
        public const double PI = 3.14159265358979;
        public double Xdiv = 0;
        public double Ydiv = 0;
        public int memory1;
        public int memory2;
        public int memory3;
        public int memory4;
        public int iter;
        public double amax;
        public double amin;
        public double pmax;
        public double pmin;
        public double[] Z; //ДН
        public double[] Z1; //ДН
        public double[] Z11; //ДН
        public double[] Z_tt;
        public double[] Z_ttt;
        public double[,] Z_tt1;
        public double[] Z_tt2;
        public double[] Z_tt3;
        public double[] Z_tt4;
        public double[] Z_tt5;
        public double[] Z_tt6;
        public double[] A; //Азимут
        public double[] Jeeves; //Элементы дживса
        public double[] P; //Место
        public double[] T; //Параметры
        public double[] R1; //Рассчётные
        public double[] R11; //Рассчётные
        public double[] R2; //Рассчётные
        public double[] R22; //Рассчётные
        public double[] SA; //Сигнал A
        public double[] SP; //Сигнал P
        public double[] pointX; //точка Х
        public double[] pointY; //точка У
        public double[] kef; //коэффициенты
        public double[] osh;// error
        public double[] fi; //ДН для функционала
        public double[] SUM; //ДН для функционала
        public double[] SUM1; //ДН для функционала

        public int realasim = 0;
        public int realplace = 0;
        double[] coor;//азимут и место для ДН
        public double index;

        //МАССИВЫ ДЛЯ ЗАПИСИ КООРДИНАТ ЗВЕЗДОЧЕК НА ВЫВОД ПОИСКА
        public double[] Xcor;
        public double[] Xcor1;
        public double[] Ycor;
        public double[] Ycor1;
        public int result = 0;
        //для хука дживса
        public int case0;
        public int case1;
        public int case2;
        public int case3;
        public int case4;

        public Functional()
        {
            
            A = new double[8];
            Jeeves = new double[6];
            P = new double[8];
            T = new double[2];
            R1 = new double[200];
            R11 = new double[200];
            R2 = new double[200];
            R22 = new double[200];
            Z = new double[R1.Length * R2.Length];
            Z1 = new double[R1.Length * R2.Length];
            Z11 = new double[R1.Length * R2.Length];
            Z_tt = new double[R1.Length * R2.Length];
            Z_ttt = new double[R1.Length * R2.Length];
            //Z_tt1 = new double[R1.Length * R2.Length][];
            Z_tt2 = new double[R1.Length * R2.Length];
            Z_tt3 = new double[R1.Length * R2.Length];
            Z_tt4 = new double[R1.Length * R2.Length];
            Z_tt5 = new double[R1.Length * R2.Length];
            Z_tt6 = new double[R1.Length * R2.Length];
            SA = new double[1];
            SP = new double[1];
            pointX = new double[1];
            pointY = new double[1];
            kef = new double[6];
            osh = new double[1];
            fi = new double[R1.Length * R2.Length];
            SUM = new double[R1.Length * R2.Length];
            SUM1 = new double[R1.Length * R2.Length];
            coor = new double[2];
            Z_tt1 = new double[R1.Length * R2.Length, 6];
            //МАССИВЫ ДЛЯ ЗАПИСИ КООРДИНАТ ЗВЕЗДОЧЕК НА ВЫВОД ПОИСКА
            Xcor = new double[200];
            //Xcor1 = new double[200];
            Ycor = new double[200];
            //Ycor1 = new double[200];

        }
        private void DoubleArray(double[] X, int n, string str)
        {
            string[] s = str.Split();
            for (int i = 0; i < n; i++)
            {
                X[i] = Convert.ToDouble(s[i]);
            }
        }

        internal void SetCoor(TextBox t1, TextBox t2)
        {
            DoubleArray(coor, 2, t1.Text + " " + t2.Text);
        }
        internal void SetA(TextBox t1, TextBox t2, TextBox t3, TextBox t4, TextBox t5, TextBox t6, TextBox t7, TextBox t8)
        {
            DoubleArray(A, 8, t1.Text + " " + t2.Text + " " + t3.Text + " " + t4.Text + " " + t5.Text + " " + t6.Text + " " + t7.Text + " " + t8.Text);
        }
        internal void SetP(TextBox t1, TextBox t2, TextBox t3, TextBox t4, TextBox t5, TextBox t6, TextBox t7, TextBox t8)
        {
            DoubleArray(P, 8, t1.Text + " " + t2.Text + " " + t3.Text + " " + t4.Text + " " + t5.Text + " " + t6.Text + " " + t7.Text + " " + t8.Text);
        }
        internal void SetT(TextBox t1, TextBox t2)
        {
            DoubleArray(T, 2, t1.Text + " " + t2.Text);
        }
        internal void SetSA(TextBox t1)
        {
            DoubleArray(SA, 1, t1.Text);
        }
        internal void SetSP(TextBox t1)
        {
            DoubleArray(SP, 1, t1.Text);
        }
        internal void SetER(TextBox t1)
        {
            DoubleArray(osh, 1, t1.Text);
        }
        internal void SetJeeves(TextBox t1, TextBox t2, TextBox t3, TextBox t4, TextBox t5, TextBox t6)
        {
            DoubleArray(Jeeves, 6, t1.Text + " " + t2.Text + " " + t3.Text + " " + t4.Text + " " + t5.Text + " " + t6.Text);
        }

        internal void Signal()
        {

            double l = 0.1;
            for (int i = 0; i < R1.Length; i++)
            {
                R1[i] = A[7] + l;
                R11[i] = A[7] + l;
                l = l + 0.1;
                if (Math.Round(SA[0], 2) == Math.Round(R1[i], 2)) realasim = i;
            }
            l = 0.1;
            for (int i = 0; i < R2.Length; i++)
            {
                R2[i] = P[7] + l;
                R22[i] = P[7] + l;
                l = l + 0.1;
                if (Math.Round(SP[0], 2) == Math.Round(R2[i], 2)) realplace = i;
            }
            l = 0;
            amin = A[7];
            amax = A[6];
            pmin = P[7];
            pmax = P[6];


            for (int k = 0; k < 6; k++)//расчет коэффициентов усиления и их зашумление по Релею через алгорим Марсальи
            {
                for (int i = 0; i < R1.Length; i++)
                {
                    for (int j = 0; j < R2.Length; j++)
                    {
                        Z_tt[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[k]) * (R1[i] - P[k]) / (2 * T[0] * T[0])) + ((R2[j] - A[k]) * (R2[j] - A[k]) / (2 * T[1] * T[1]))));

                        if (Z[i * R2.Length + j] < Z_tt[i * R2.Length + j])
                        {
                            Z[i * R2.Length + j] = Z_tt[i * R2.Length + j];
                            //Z1[i * R2.Length + j] = Z_tt[i * R2.Length + j];
                        }

                        double X; double Y; double N; double deviation; double expectation;
                        var rand = new Random();

                        if (i == realplace)
                        {
                            if (j == realasim)
                            {
                                kef[k] = Z_tt[i * R2.Length + j];

                                //расчёт дисперсии 

                                deviation = (1 / Math.Sqrt(2)) * Math.Pow(10, -(osh[0] / 20));
                                expectation = Math.Sqrt(deviation);

                                NormalRandom nr = new NormalRandom();
                                X = nr.NextDouble() * deviation + expectation;
                                Y = nr.NextDouble() * deviation + expectation;
                                N = Math.Sqrt(X * X + Y * Y);
                                //Если нужно получить нормальное распределение с другими μ и σ, применяйте скалирование:
                                //double x = nr.NextDouble() * deviation + expectation;

                                kef[k] = kef[k] + N;

                            }
                        }

                    }
                }
            }//расчет коэффициентов усиления и их зашумление по Релею через алгорим Марсальи



            for (int z = 0; z < 6; z++)
            {
                for (int i = 0; i < R1.Length; i++)
                {
                    for (int j = 0; j < R2.Length; j++)
                    {

                        Z_tt1[i * R2.Length + j, z] = Math.Exp(-(((R1[i] - P[z]) * (R1[i] - P[z]) / (2 * T[0] * T[0])) + ((R2[j] - A[z]) * (R2[j] - A[z]) / (2 * T[1] * T[1]))));

                    }
                }
            }
            /*for (int i = 0; i < R1.Length; i++)
            {
                for (int j = 0; j < R2.Length; j++)
                {
                    Z_tt2[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[1]) * (R1[i] - P[1]) / (2 * T[0] * T[0])) + ((R2[j] - A[1]) * (R2[j] - A[1]) / (2 * T[1] * T[1]))));
                }
            }
            for (int i = 0; i < R1.Length; i++)
            {
                for (int j = 0; j < R2.Length; j++)
                {
                    Z_tt3[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[2]) * (R1[i] - P[2]) / (2 * T[0] * T[0])) + ((R2[j] - A[2]) * (R2[j] - A[2]) / (2 * T[1] * T[1]))));
                }
            }
            for (int i = 0; i < R1.Length; i++)
            {
                for (int j = 0; j < R2.Length; j++)
                {
                    Z_tt4[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[3]) * (R1[i] - P[3]) / (2 * T[0] * T[0])) + ((R2[j] - A[3]) * (R2[j] - A[3]) / (2 * T[1] * T[1]))));
                }
            }
            for (int i = 0; i < R1.Length; i++)
            {
                for (int j = 0; j < R2.Length; j++)
                {
                    Z_tt5[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[4]) * (R1[i] - P[4]) / (2 * T[0] * T[0])) + ((R2[j] - A[4]) * (R2[j] - A[4]) / (2 * T[1] * T[1]))));
                }
            }
            for (int i = 0; i < R1.Length; i++)
            {
                for (int j = 0; j < R2.Length; j++)
                {
                    Z_tt6[i * R2.Length + j] = Math.Exp(-(((R1[i] - P[5]) * (R1[i] - P[5]) / (2 * T[0] * T[0])) + ((R2[j] - A[5]) * (R2[j] - A[5]) / (2 * T[1] * T[1]))));
                }
            }*/




            /*for (int x = 0; x < 6; x++)//расчет функционала
            {
                for (int y = 0; y < 6; y++)
                {
                    if (y != x)
                    {
                        for (int i = 0; i < R1.Length; i++)
                        {
                            for (int j = 0; j < R2.Length; j++)
                            {
                                Z_ttt[i * R2.Length + j] =
                                    Math.Pow((kef[x] - kef[y]) / (kef[x] + kef[y]) -
                                    (Math.Exp(-(((R1[i] - P[x]) * (R1[i] - P[x]) / (2 * T[0] * T[0])) + ((R2[j] - A[x]) * (R2[j] - A[x]) / (2 * T[1] * T[1])))) -
                                    Math.Exp(-(((R1[i] - P[y]) * (R1[i] - P[y]) / (2 * T[0] * T[0])) + ((R2[j] - A[y]) * (R2[j] - A[y]) / (2 * T[1] * T[1]))))) /
                                    (Math.Exp(-(((R1[i] - P[x]) * (R1[i] - P[x]) / (2 * T[0] * T[0])) + ((R2[j] - A[x]) * (R2[j] - A[x]) / (2 * T[1] * T[1])))) +
                                    Math.Exp(-(((R1[i] - P[y]) * (R1[i] - P[y]) / (2 * T[0] * T[0])) + ((R2[j] - A[y]) * (R2[j] - A[y]) / (2 * T[1] * T[1]))))), 2);

                                if (Z11[i * R2.Length + j] < Z_ttt[i * R2.Length + j])
                                {
                                    Z11[i * R2.Length + j] = Z_ttt[i * R2.Length + j];
                                }

                            }
                        }
                    }
                }

            }*/


            /////----------------второй рабочий вариант


            /*for (int r = 0; r < 6; r++)
            {
                for (int k = r + 1; k < 6; k++)
                {
                    if (r != k)
                    {
                        for (int i = 0; i < R1.Length; i++)
                        {
                            double x = (R1[i] - P[k]) * (R1[i] - P[k]) / (2 * T[0] * T[0]);
                            double x1 = (R1[i] - P[r]) * (R1[i] - P[r]) / (2 * T[0] * T[0]);

                            for (int j = 0; j < R2.Length; j++)
                            {
                                double y = (R2[j] - A[k]) * (R2[j] - A[k]) / (2 * T[1] * T[1]);
                                double y1 = (R2[j] - A[r]) * (R2[j] - A[r]) / (2 * T[1] * T[1]);


                                Z_ttt[i * R2.Length + j] = Math.Pow((kef[r] - kef[k]) / (kef[r] + kef[k]) -
                                    (Math.Exp(-(x + y)) - Math.Exp(-(x1 + y1))) / (Math.Exp(-(x + y)) + Math.Exp(-(x1 + y1))), 2);

                                if (Z11[i * R2.Length + j] < Z_ttt[i * R2.Length + j])
                                {
                                    Z11[i * R2.Length + j] = Z_ttt[i * R2.Length + j];
                                }
                            }
                        }
                    }
                }
            }*/

            /////--------------для Raschet


            /*for(int i = 0; i < R1.Length;i++)
            {
                double tik = Raschet();
                Z_ttt[i] = tik;
            }*/

            /////------------третий вариант

            double sum;
            for (int i = 0; i < 6; i++)
            {
                for (int j = i + 1; j < 6; j++)
                {
                    if (i != j)
                    {
                        for (int k = 0; k < R1.Length; k++)
                        {
                            for (int t = 0; t < R2.Length; t++)
                            {


                                sum = ((Z_tt1[k * R2.Length + t, i] - Z_tt1[k * R2.Length + t, j]) /
                                (Z_tt1[k * R2.Length + t, i] + Z_tt1[k * R2.Length + t, j]) -
                                (kef[i] - kef[j]) / (kef[i] + kef[j])) *
                                ((Z_tt1[k * R2.Length + t, i] - Z_tt1[k * R2.Length + t, j]) /
                                (Z_tt1[k * R2.Length + t, i] + Z_tt1[k * R2.Length + t, j]) -
                                (kef[i] - kef[j]) / (kef[i] + kef[j]));

                                SUM[k * R2.Length + t] += sum;
                                //sum = 0;
                            }
                        }

                    }
                }
            }


            for (int r = 0; r < R2.Length; r++)
            {
                for (int n = 0; n < R2.Length; n++)
                {
                    /*double sum = 0;
                    for (int i = 0; i < 6; i++)
                    {
                        for (int j = i + 1; j < 6; j++)
                        {
                            if (i != j)
                            {
                                for (int k = 0; k < R1.Length; k++)
                                {
                                    for (int t = 0; t < R2.Length; t++)
                                    {
                                        

                                        sum += ((Z_tt1[k * R2.Length + t, i] - Z_tt1[k * R2.Length + t, j]) /
                                        (Z_tt1[k * R2.Length + t, i] + Z_tt1[k * R2.Length + t, j]) -
                                        (kef[i] - kef[j]) / (kef[i] + kef[j])) *
                                        ((Z_tt1[k * R2.Length + t, i] - Z_tt1[k * R2.Length + t, j]) /
                                        (Z_tt1[k * R2.Length + t, i] + Z_tt1[k * R2.Length + t, j]) -
                                        (kef[i] - kef[j]) / (kef[i] + kef[j]));
                                    }
                                }

                            }
                        }
                    }*/
                    Z_ttt[r * R2.Length + n] = SUM[r * R2.Length + n];
                    if (Z11[r * R2.Length + n] < Z_ttt[r * R2.Length + n])
                    {
                        Z11[r * R2.Length + n] = Z_ttt[r * R2.Length + n];
                    }
                    //sum = 0;
                }

            }

            //------------------------Функция для метода Хука - Дживса


            //index = Raschet(coor[0], coor[1]);

            MHJ();
            index = Raschet(0, 0);
            Xcor1 = new double[iter];
            Ycor1 = new double[iter];
            for (int i = 0; i < iter; i++)
            {
                //if(Xcoor[i]==0 && Ycoor[i] == 0) { Xc[i] = Xcoor[i]; }
                Xcor1[i] = Xcor[i];
                //Xcor1[iter-1]
                
            }
            for (int i = 0; i < iter; i++)
            {
                //if(Xcoor[i]==0 && Ycoor[i] == 0) { Xc[i] = Xcoor[i]; }
                Ycor1[i] = Ycor[i];

            }
            pointX[0] = Xcor1[iter - 1];
            pointY[0] = Ycor1[iter - 1];

            /*for(int i = 0;i<iter-1;i++)
            {
                Xcor1[i] = Xcor[i];
                Ycor1[i] = Ycor[i];
            }*/

            //-----------------------что то из инета
            /*
            //Console.WriteLine("Метод Хука - Дживиса");
            //Console.WriteLine("Введите начальную точку X");
            //double x0 = Convert.ToDouble(Console.ReadLine());
            double x0 = 123.12;
            //Console.WriteLine("Введите начальную точку Y");
            //double y0 = Convert.ToDouble(Console.ReadLine());
            double y0 = 125.7;
            //Console.WriteLine("Введите точность");
            //double e = Convert.ToDouble(Console.ReadLine());
            double e = 0.1;
            //Console.WriteLine("Введите шаг");
            //double step = Convert.ToDouble(Console.ReadLine());
            double step = 1.32;
            //Console.WriteLine("Введите ускорение");
            //double acc = Convert.ToDouble(Console.ReadLine());
            double acc = 7;
            //Console.WriteLine("Введите коэффициент уменьшения шага");
            //double decrese = Convert.ToDouble(Console.ReadLine());
            double decrese = 1.73;
            
            double x = x0;
            double y = y0;
            double step_x = step;
            double step_y = step;
            bool x_bool, y_bool;
            double new_x = 0, new_y = 0;
            while ((step_x >= e) | (step_y >= e))
            {

                if (Raschet(x, y) > Raschet(x - step_x, y))
                {
                    x = x - step_x;
                    new_x = x + step_x;
                    x_bool = true;


                }
                else
                {
                    if (Raschet(x, y) > Raschet(x + step_x, y))
                    {
                        x = x + step_x;
                        new_x = x - step_x;
                        x_bool = true;
                    }

                }
                if (Raschet(x, y) > Raschet(x, y - step_y))
                {
                    y = y - step_y;
                    new_y = y + step_y;
                    y_bool = true;
                }
                else
                {
                    if (Raschet(x, y) > Raschet(x, y + step_y))
                    {
                        y = y + step_y;
                        new_y = y - step_y;
                        y_bool = true;
                    }
                }
                if ((x_bool = true) && (y_bool = true))
                {
                    x = x + acc * (x - new_x);
                    y = y + acc * (y - new_y);
                    Console.WriteLine("X is: " + Convert.ToString(x) + " Y is: " + Convert.ToString(y));
                    x_bool = false;
                    y_bool = false;
                }
                else
                {
                    x_bool = false;
                    y_bool = false;

                }

                if ((Raschet(x, y) <= Raschet(x + step_x, y)) && (Raschet(x - step_x, y) >= Raschet(x, y)))
                {
                    step_x = step_x / decrese;
                }
                if ((Raschet(x, y) <= Raschet(x, y + step_y)) & (Raschet(x, y - step_y) >= Raschet(x, y)))
                {
                    step_y = step_y / decrese;
                }
                //Console.WriteLine("X is: " + Convert.ToString(x));
                //Console.WriteLine("Y is: " + Convert.ToString(y));
                //Console.WriteLine("step_x is:" + Convert.ToString(step_x) + "\n\r" + " step_y is:" + Convert.ToString(step_y));



            }*/

        }

        internal double function(double az, double pl, int i)
        {
            double SUM;


            SUM = Math.Exp(-(((pl - P[i]) * (pl - P[i]) / (2 * T[0] * T[0])) + ((az - A[i]) * (az - A[i]) / (2 * T[1] * T[1]))));


            return SUM;
        }


        internal double Raschet(double pl, double az)
        {
            double SUM=0;

            for (int i = 0; i < 6; i++)
            {
                for (int j = i + 1; j < 6; j++)
                {
                    if (i != j)
                    {
                        SUM += ((function(az, pl, i) - function(az, pl, j)) / (function(az, pl, i) + function(az, pl, j)) - (kef[i] - kef[j]) / (kef[i] + kef[j])) *
                            ((function(az, pl, i) - function(az, pl, j)) / (function(az, pl, i) + function(az, pl, j)) - (kef[i] - kef[j]) / (kef[i] + kef[j]));
                    }
                }
            }
            return SUM;

        }


        //Исследующий поиск сюда
        internal void issled_poisk(double x, double y, double hx, double hy, double e, double l)
        {

            if (x > 0 && y > 0)
            {
                if (Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case1 = 1;
                }
                else if (Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1;
                }
                else if (Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case2 = 1;
                }
                else if (Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case0 = 1;
                }


                if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }
            }
            if (x > 0 && y < 0)
            {
                if (Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case2 = 1;
                }
                else if (Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case0 = 1;
                }
                else if (Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1;
                }
                else if (Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case1 = 1;
                }



                if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }

            }
            if (x < 0 && y < 0)
            {
                if (Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case0 = 1;
                }
                else if (Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case2 = 1;
                }
                else if (Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1;
                }
                else if (Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case1 = 1;
                }



                if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }

            }
            if (x < 0 && y > 0)
            {
                if (Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1;
                }
                else if (Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case0 = 1;
                }
                else if (Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case1 = 1;
                }
                else if (Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case2 = 1;
                }



                /*if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }*/

            }
        }
        internal void issled_poisk1(double x, double y, double hx, double hy, double e, double l,double timecase)
        {

            if (x > 0 && y > 0)
            {
                if (timecase != 1 && Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 1; case2 = 0;
                }
                else if (timecase != 3 && Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1; case0 = 0; case1 = 0; case2 = 0;
                }
                else if (timecase != 2 && Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 0; case2 = 1;
                }
                else if (timecase != 0 && Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 1; case1 = 0; case2 = 0;
                }


                if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }
            }
            if (x > 0 && y < 0)
            {
                if (timecase != 2 && Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 0; case2 = 1;
                }
                else if (timecase != 0 && Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 1; case1 = 0; case2 = 0;
                }
                else if (timecase != 3 && Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1; case0 = 0; case1 = 0; case2 = 0;
                }
                else if (timecase != 1 && Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 1; case2 = 0;
                }



                if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }

            }
            if (x < 0 && y < 0)
            {
                if (timecase != 0 && Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 1; case1 = 0; case2 = 0;
                }
                else if (timecase != 2 && Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 0; case2 = 1;
                }
                else if (timecase != 3 && Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1; case0 = 0; case1 = 0; case2 = 0;
                }
                else if (timecase != 1 && Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 1; case2 = 0;
                }



                if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }

            }
            if (x < 0 && y > 0)
            {
                if (timecase != 3 && Raschet(y - hy, x + hx) < Raschet(y, x))
                {
                    case3 = 1;case0 = 0;case1 = 0;case2 = 0;
                }
                else if (timecase != 0 && Raschet(y + hx, x + hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 1; case1 = 0; case2 = 0;
                }
                else if (timecase != 1 && Raschet(y - hx, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 1; case2 = 0;
                }
                else if (timecase != 2 && Raschet(y + hy, x - hx) < Raschet(y, x))
                {
                    case3 = 0; case0 = 0; case1 = 0; case2 = 1;
                }



                /*if (e < Math.Sqrt(hx * hx + hy * hy))
                {
                    result = 1;
                    //return;
                }
                else
                {
                    case4 = 1;
                }*/

            }
        }


        internal void MHJ()
        {

            //int i = 0;
            //int j = 0;
            double timecase = 0;
            double fxk = 10000;
            double fxk1 = 0;
            double x = 0;
            double y = 0;
            double xk1 = 0;
            double xk0 = 0;
            double yk1 = 0;
            double yk0 = 0;
            double xs = 0;
            double ys = 0;
            iter = 0;

            //начальное приближение, пока задаем руками

            double x0 = Jeeves[0];
            double y0 = Jeeves[1];
            Random rnd = new Random();
            int xr = rnd.Next(-4, 4);
            int yr = rnd.Next(-4, 4);
            if (xr == 0) { xr = 3; }
            if (yr == 0) { yr = 3; }
            x0 = xr;
            y0 = yr;
            //шаги по х и по у, их величину тоже пока задаем руками и смотрим на сколько нужно скорректировать, изначально возьму по 0,5

            double hx = Jeeves[2];
            double hy = Jeeves[3];

            //ошибка измерения, чтобы алгоритм быстро остановился ошибку изначально выберем большой и тоже зададим руками, изначально 0,5, главное - меньше 1

            double e = Jeeves[4];

            //коэффициент уменьшения шага альфа, главное больше 1, обычно берут за 2, все пока вручную

            double l = Jeeves[5];

            //инициализируем начальную точку
            xk0 = x0;
            yk0 = y0;
            xk1 = x0;
            yk1 = y0;
            //Записали базовую точку
            Xcor[iter] = xk0;
            Ycor[iter] = yk0;

            int select = 1;

            switch (select)
            {
                case 1://исследующий поиск
                    if (iter == 199) break;
                    xs = xk0;ys = yk0;
                    xk0 = xk1; yk0 = yk1;
                    issled_poisk(xk0, yk0, hx, hy, e, l);
                    
                    if (case0 == 1)
                    {
                        x = xk0 + hx;
                        y = yk0 + hy;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk) 
                        {
                            if (e > Math.Sqrt(hx * hx + hy * hy))
                            {
                                goto case 3;
                            }
                            timecase = 0;
                            goto case 4;
                            //goto case 3; 
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (case1 == 1)
                    {
                        x = xk0 - hx;
                        y = yk0 - hy;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk) 
                        {
                            if (e > Math.Sqrt(hx * hx + hy * hy))
                            {
                                goto case 3;
                            }
                            timecase = 1;
                            goto case 4;
                            //goto case 3; 
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (case2 == 1)
                    {
                        y = yk0 + hy;
                        x = xk0 - hx;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk) 
                        {
                            if (e > Math.Sqrt(hx * hx + hy * hy))
                            {
                                goto case 3;
                            }
                            timecase = 2;
                            goto case 4;
                            //goto case 3; 
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (case3 == 1)
                    {
                        y = yk0 - hy;
                        x = xk0 + hx;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk) 
                        {
                            if (e > Math.Sqrt(hx * hx + hy * hy))
                            {
                                goto case 3;
                            }
                            timecase = 3;
                            goto case 4;
                            //goto case 3; 
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (result == 1)
                    {
                        goto case 3;
                    }
                    else if (case4 == 1)
                    {
                        hx /= l; hy /= l;
                        goto case 1;
                    }
                    break;


                case 2://поиск по образцу xpk+1=xk+(xk-xk-1).
                    xk1 = x + l*(x - xk0);
                    yk1 = y + l*(y - yk0);
                    fxk = Raschet(yk1, xk1);
                    goto case 1;

                case 3:
                    //if (e > Math.Sqrt(hx * hx + hy * hy))
                    //if (e >(hx * hx + hy * hy))
                    if (e < Math.Sqrt(Raschet(y,x)* Raschet(y, x)-Raschet(ys,xs)*Raschet(ys,xs)))
                    //if (e < Raschet(y, x)  - Raschet(ys, xs) )
                    //if (e > 10000)
                    {
                        //x = xk0;
                        //y = yk0;
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        return;
                    }
                    else 
                    { 
                        hx /= l; hy /= l;
                        goto case 1;
                    }
                case 4:
                    //исследующий поиск прошлой точки
                    //if (iter == 199) break;
                    //xs = xk0; ys = yk0;
                    xk0 = xk1; yk0 = yk1;
                    issled_poisk1(xk0, yk0, hx, hy, e, l,timecase);

                    if (case0 == 1)
                    {
                        x = xk0 + hx;
                        y = yk0 + hy;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk)
                        {
                            goto case 3;
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (case1 == 1)
                    {
                        x = xk0 - hx;
                        y = yk0 - hy;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk)
                        {
                            goto case 3;
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (case2 == 1)
                    {
                        y = yk0 + hy;
                        x = xk0 - hx;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk)
                        {
                            goto case 3;
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (case3 == 1)
                    {
                        y = yk0 - hy;
                        x = xk0 + hx;
                        fxk1 = Raschet(y, x);
                        if (fxk1 > fxk)
                        {
                            goto case 3;
                        }
                        iter++;
                        Xcor[iter] = x;
                        Ycor[iter] = y;
                        goto case 2;
                    }
                    else if (result == 1)
                    {
                        goto case 3;
                    }
                    else if (case4 == 1)
                    {
                        hx /= l; hy /= l;
                        goto case 1;
                    }
                    break;


            }

            


        }


        internal double[] GetCoorX()
        {
            return Xcor1;
        }
        internal double[] GetCoorY()
        {
            return Ycor1;
        }
        internal double[] GetX()
        {
            //write(Ycor);
            //write1(Xcor);
            return R1;
        }
        internal double[] GetY()
        {
            return R2;
        }
        internal double[] GetZ()
        {
            return Z11;
            //return Z_ttt;
        }
        internal double[] GetX1()
        {
            //write(test);
            return R11;
        }
        internal double[] GetY1()
        {
            return R22;
        }
        internal double[] GetZ1()
        {
            return Z1;
        }
        internal double[] GetSA()
        {
            return SA;
        }
        internal double[] GetSP()
        {
            return SP;
        }
        internal double[] GetpointX()
        {
            return pointX;
        }
        internal double[] GetpointY()
        {
            return pointY;
        }
        internal double getAmax()
        {
            return A[6];
        }
        internal double getPmax()
        {
            return P[6];
        }
        internal double getAmin()
        {
            return A[7];
        }
        internal double getPmin()
        {
            return P[7];
        }
        internal double getAmax1()
        {
            return amax;
        }
        internal double getPmax1()
        {
            return pmax;
        }
        internal double getAmin1()
        {
            return amin;
        }
        internal double getPmin1()
        {
            return pmin;
        }
        internal double getkef1()
        {
            return kef[0];
            //return Xcor[iter];
            //return iter;
            //return index;

        }
        internal double getkef2()
        {
            return kef[1];
            //return Ycor[iter];
           
        }
        internal double getkef3()
        {
            return kef[2];
            //return releyerr[2];
            //return index;
        }
        internal double getkef4()
        {
            return kef[3];
            //return releyerr[3];
        }
        internal double getkef5()
        {
            return kef[4];
            //return releyerr[4];
        }
        internal double getkef6()
        {
            return kef[5];
            //return releyerr[5];
        }
        internal double getdivX()
        {
            return SA[0] - Xcor1[iter-1];
            //return releyerr[5];
        }
        internal double getdivY()
        {
            return SP[0] - Ycor1[iter-1];
            //return releyerr[5];
        }
        
            internal int getiter()
        {
            return iter;
            //return releyerr[5];
        }

        /*private void write(double[] X)
        {
            System.IO.StreamWriter textFile = new System.IO.StreamWriter(@"D:\Diplom\Kyrsach\test.txt");
            for (int i = 0; i < X.Length; i++)
            {
                
                textFile.WriteLine(X[i]);
            }
            textFile.Close();
        }
        private void write1(double[] X)
        {
            System.IO.StreamWriter textFile = new System.IO.StreamWriter(@"D:\Diplom\Kyrsach\test1.txt");
            for (int i = 0; i < X.Length; i++)
            {

                textFile.WriteLine(X[i]);
            }
            textFile.Close();
        }*/




    }
}
