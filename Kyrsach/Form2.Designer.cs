﻿namespace Kyrsach
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Create_button = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.chartViewer2 = new ChartDirector.WinChartViewer();
            this.azim = new System.Windows.Forms.TextBox();
            this.place = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.A1 = new System.Windows.Forms.TextBox();
            this.A6 = new System.Windows.Forms.TextBox();
            this.A5 = new System.Windows.Forms.TextBox();
            this.A2 = new System.Windows.Forms.TextBox();
            this.A4 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.P6 = new System.Windows.Forms.TextBox();
            this.P5 = new System.Windows.Forms.TextBox();
            this.P4 = new System.Windows.Forms.TextBox();
            this.P3 = new System.Windows.Forms.TextBox();
            this.P1 = new System.Windows.Forms.TextBox();
            this.P2 = new System.Windows.Forms.TextBox();
            this.Amin = new System.Windows.Forms.TextBox();
            this.Amax = new System.Windows.Forms.TextBox();
            this.Pmin = new System.Windows.Forms.TextBox();
            this.Pmax = new System.Windows.Forms.TextBox();
            this.T1 = new System.Windows.Forms.TextBox();
            this.T2 = new System.Windows.Forms.TextBox();
            this.PS = new System.Windows.Forms.TextBox();
            this.AS = new System.Windows.Forms.TextBox();
            this.Kef6 = new System.Windows.Forms.TextBox();
            this.Kef5 = new System.Windows.Forms.TextBox();
            this.Kef4 = new System.Windows.Forms.TextBox();
            this.Kef3 = new System.Windows.Forms.TextBox();
            this.Kef2 = new System.Windows.Forms.TextBox();
            this.Kef1 = new System.Windows.Forms.TextBox();
            this.osh = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.L = new System.Windows.Forms.TextBox();
            this.E = new System.Windows.Forms.TextBox();
            this.HY = new System.Windows.Forms.TextBox();
            this.HX = new System.Windows.Forms.TextBox();
            this.Y0 = new System.Windows.Forms.TextBox();
            this.X0 = new System.Windows.Forms.TextBox();
            this.DivX = new System.Windows.Forms.TextBox();
            this.DivY = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Create_button
            // 
            this.Create_button.Location = new System.Drawing.Point(945, 564);
            this.Create_button.Name = "Create_button";
            this.Create_button.Size = new System.Drawing.Size(178, 65);
            this.Create_button.TabIndex = 0;
            this.Create_button.Text = "Построить функционал";
            this.Create_button.UseVisualStyleBackColor = true;
            this.Create_button.Click += new System.EventHandler(this.Create_button_Click);
            // 
            // chartViewer2
            // 
            this.chartViewer2.Location = new System.Drawing.Point(12, 12);
            this.chartViewer2.Name = "chartViewer2";
            this.chartViewer2.Size = new System.Drawing.Size(316, 237);
            this.chartViewer2.TabIndex = 1;
            this.chartViewer2.TabStop = false;
            // 
            // azim
            // 
            this.azim.Location = new System.Drawing.Point(945, 26);
            this.azim.Name = "azim";
            this.azim.Size = new System.Drawing.Size(100, 20);
            this.azim.TabIndex = 2;
            this.azim.Text = "0";
            // 
            // place
            // 
            this.place.Location = new System.Drawing.Point(945, 52);
            this.place.Name = "place";
            this.place.Size = new System.Drawing.Size(100, 20);
            this.place.TabIndex = 3;
            this.place.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(919, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "азимут";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1017, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "место";
            // 
            // A1
            // 
            this.A1.Location = new System.Drawing.Point(670, 12);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(66, 20);
            this.A1.TabIndex = 6;
            this.A1.Text = "-2,33";
            // 
            // A6
            // 
            this.A6.Location = new System.Drawing.Point(670, 142);
            this.A6.Name = "A6";
            this.A6.Size = new System.Drawing.Size(66, 20);
            this.A6.TabIndex = 11;
            this.A6.Text = "0";
            // 
            // A5
            // 
            this.A5.Location = new System.Drawing.Point(670, 116);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(66, 20);
            this.A5.TabIndex = 10;
            this.A5.Text = "2,33";
            // 
            // A2
            // 
            this.A2.Location = new System.Drawing.Point(670, 38);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(66, 20);
            this.A2.TabIndex = 7;
            this.A2.Text = "-1,6";
            // 
            // A4
            // 
            this.A4.Location = new System.Drawing.Point(670, 90);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(66, 20);
            this.A4.TabIndex = 9;
            this.A4.Text = "1,6";
            // 
            // A3
            // 
            this.A3.Location = new System.Drawing.Point(670, 64);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(66, 20);
            this.A3.TabIndex = 8;
            this.A3.Text = "0";
            // 
            // P6
            // 
            this.P6.Location = new System.Drawing.Point(742, 141);
            this.P6.Name = "P6";
            this.P6.Size = new System.Drawing.Size(66, 20);
            this.P6.TabIndex = 17;
            this.P6.Text = "0";
            // 
            // P5
            // 
            this.P5.Location = new System.Drawing.Point(742, 115);
            this.P5.Name = "P5";
            this.P5.Size = new System.Drawing.Size(66, 20);
            this.P5.TabIndex = 16;
            this.P5.Text = "0,73";
            // 
            // P4
            // 
            this.P4.Location = new System.Drawing.Point(742, 89);
            this.P4.Name = "P4";
            this.P4.Size = new System.Drawing.Size(66, 20);
            this.P4.TabIndex = 15;
            this.P4.Text = "-1,8";
            // 
            // P3
            // 
            this.P3.Location = new System.Drawing.Point(742, 64);
            this.P3.Name = "P3";
            this.P3.Size = new System.Drawing.Size(66, 20);
            this.P3.TabIndex = 14;
            this.P3.Text = "2,4";
            // 
            // P1
            // 
            this.P1.Location = new System.Drawing.Point(742, 12);
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(66, 20);
            this.P1.TabIndex = 12;
            this.P1.Text = "0,73";
            // 
            // P2
            // 
            this.P2.Location = new System.Drawing.Point(742, 38);
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(66, 20);
            this.P2.TabIndex = 13;
            this.P2.Text = "-1,8";
            // 
            // Amin
            // 
            this.Amin.Location = new System.Drawing.Point(658, 306);
            this.Amin.Name = "Amin";
            this.Amin.Size = new System.Drawing.Size(78, 20);
            this.Amin.TabIndex = 20;
            this.Amin.Text = "-5";
            this.Amin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Amax
            // 
            this.Amax.Location = new System.Drawing.Point(658, 267);
            this.Amax.Name = "Amax";
            this.Amax.Size = new System.Drawing.Size(78, 20);
            this.Amax.TabIndex = 18;
            this.Amax.Text = "5";
            this.Amax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pmin
            // 
            this.Pmin.Location = new System.Drawing.Point(742, 306);
            this.Pmin.Name = "Pmin";
            this.Pmin.Size = new System.Drawing.Size(75, 20);
            this.Pmin.TabIndex = 21;
            this.Pmin.Text = "-5";
            this.Pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pmax
            // 
            this.Pmax.Location = new System.Drawing.Point(742, 267);
            this.Pmax.Name = "Pmax";
            this.Pmax.Size = new System.Drawing.Size(75, 20);
            this.Pmax.TabIndex = 19;
            this.Pmax.Text = "5";
            this.Pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // T1
            // 
            this.T1.Location = new System.Drawing.Point(705, 433);
            this.T1.Name = "T1";
            this.T1.Size = new System.Drawing.Size(103, 20);
            this.T1.TabIndex = 23;
            this.T1.Text = "2";
            this.T1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // T2
            // 
            this.T2.Location = new System.Drawing.Point(705, 497);
            this.T2.Name = "T2";
            this.T2.Size = new System.Drawing.Size(103, 20);
            this.T2.TabIndex = 22;
            this.T2.Text = "2";
            this.T2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PS
            // 
            this.PS.Location = new System.Drawing.Point(998, 200);
            this.PS.Name = "PS";
            this.PS.Size = new System.Drawing.Size(103, 20);
            this.PS.TabIndex = 25;
            this.PS.Text = "1";
            this.PS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AS
            // 
            this.AS.Location = new System.Drawing.Point(888, 200);
            this.AS.Name = "AS";
            this.AS.Size = new System.Drawing.Size(103, 20);
            this.AS.TabIndex = 24;
            this.AS.Text = "1";
            this.AS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Kef6
            // 
            this.Kef6.Location = new System.Drawing.Point(979, 396);
            this.Kef6.Name = "Kef6";
            this.Kef6.Size = new System.Drawing.Size(100, 20);
            this.Kef6.TabIndex = 31;
            // 
            // Kef5
            // 
            this.Kef5.Location = new System.Drawing.Point(979, 370);
            this.Kef5.Name = "Kef5";
            this.Kef5.Size = new System.Drawing.Size(100, 20);
            this.Kef5.TabIndex = 30;
            // 
            // Kef4
            // 
            this.Kef4.Location = new System.Drawing.Point(979, 345);
            this.Kef4.Name = "Kef4";
            this.Kef4.Size = new System.Drawing.Size(100, 20);
            this.Kef4.TabIndex = 29;
            // 
            // Kef3
            // 
            this.Kef3.Location = new System.Drawing.Point(979, 319);
            this.Kef3.Name = "Kef3";
            this.Kef3.Size = new System.Drawing.Size(100, 20);
            this.Kef3.TabIndex = 28;
            // 
            // Kef2
            // 
            this.Kef2.Location = new System.Drawing.Point(979, 293);
            this.Kef2.Name = "Kef2";
            this.Kef2.Size = new System.Drawing.Size(100, 20);
            this.Kef2.TabIndex = 27;
            // 
            // Kef1
            // 
            this.Kef1.Location = new System.Drawing.Point(979, 267);
            this.Kef1.Name = "Kef1";
            this.Kef1.Size = new System.Drawing.Size(100, 20);
            this.Kef1.TabIndex = 26;
            // 
            // osh
            // 
            this.osh.Location = new System.Drawing.Point(966, 461);
            this.osh.Name = "osh";
            this.osh.Size = new System.Drawing.Size(100, 20);
            this.osh.TabIndex = 32;
            this.osh.Text = "20";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.L);
            this.groupBox1.Controls.Add(this.E);
            this.groupBox1.Controls.Add(this.HY);
            this.groupBox1.Controls.Add(this.HX);
            this.groupBox1.Controls.Add(this.Y0);
            this.groupBox1.Controls.Add(this.X0);
            this.groupBox1.Location = new System.Drawing.Point(1183, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(207, 267);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Метод Хука - Дживса";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 188);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(9, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "l";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "e";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "hy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "hx";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "y0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "x0";
            // 
            // L
            // 
            this.L.Location = new System.Drawing.Point(92, 181);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(100, 20);
            this.L.TabIndex = 5;
            this.L.Text = "2";
            // 
            // E
            // 
            this.E.Location = new System.Drawing.Point(92, 155);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(100, 20);
            this.E.TabIndex = 4;
            this.E.Text = "0,0005";
            // 
            // HY
            // 
            this.HY.Location = new System.Drawing.Point(92, 129);
            this.HY.Name = "HY";
            this.HY.Size = new System.Drawing.Size(100, 20);
            this.HY.TabIndex = 3;
            this.HY.Text = "0,05";
            // 
            // HX
            // 
            this.HX.Location = new System.Drawing.Point(92, 103);
            this.HX.Name = "HX";
            this.HX.Size = new System.Drawing.Size(100, 20);
            this.HX.TabIndex = 2;
            this.HX.Text = "0,05";
            // 
            // Y0
            // 
            this.Y0.Location = new System.Drawing.Point(92, 64);
            this.Y0.Name = "Y0";
            this.Y0.ReadOnly = true;
            this.Y0.Size = new System.Drawing.Size(100, 20);
            this.Y0.TabIndex = 1;
            this.Y0.Text = "4";
            // 
            // X0
            // 
            this.X0.Location = new System.Drawing.Point(92, 32);
            this.X0.Name = "X0";
            this.X0.ReadOnly = true;
            this.X0.Size = new System.Drawing.Size(100, 20);
            this.X0.TabIndex = 0;
            this.X0.Text = "2";
            // 
            // DivX
            // 
            this.DivX.Location = new System.Drawing.Point(1265, 333);
            this.DivX.Name = "DivX";
            this.DivX.Size = new System.Drawing.Size(100, 20);
            this.DivX.TabIndex = 34;
            // 
            // DivY
            // 
            this.DivY.Location = new System.Drawing.Point(1265, 359);
            this.DivY.Name = "DivY";
            this.DivY.Size = new System.Drawing.Size(100, 20);
            this.DivY.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1168, 336);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Отклонение по Х";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1168, 362);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "Отклонение по Y";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1436, 674);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DivY);
            this.Controls.Add(this.DivX);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.osh);
            this.Controls.Add(this.Kef6);
            this.Controls.Add(this.Kef5);
            this.Controls.Add(this.Kef4);
            this.Controls.Add(this.Kef3);
            this.Controls.Add(this.Kef2);
            this.Controls.Add(this.Kef1);
            this.Controls.Add(this.PS);
            this.Controls.Add(this.AS);
            this.Controls.Add(this.T1);
            this.Controls.Add(this.T2);
            this.Controls.Add(this.Amin);
            this.Controls.Add(this.Amax);
            this.Controls.Add(this.Pmin);
            this.Controls.Add(this.Pmax);
            this.Controls.Add(this.P6);
            this.Controls.Add(this.P5);
            this.Controls.Add(this.P4);
            this.Controls.Add(this.P3);
            this.Controls.Add(this.P1);
            this.Controls.Add(this.P2);
            this.Controls.Add(this.A6);
            this.Controls.Add(this.A5);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A4);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.place);
            this.Controls.Add(this.azim);
            this.Controls.Add(this.chartViewer2);
            this.Controls.Add(this.Create_button);
            this.Name = "Form2";
            this.Text = "Курсовая работа. Долганов.";
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Create_button;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private ChartDirector.WinChartViewer chartViewer2;
        private System.Windows.Forms.TextBox azim;
        private System.Windows.Forms.TextBox place;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.TextBox A6;
        private System.Windows.Forms.TextBox A5;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox A4;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.TextBox P6;
        private System.Windows.Forms.TextBox P5;
        private System.Windows.Forms.TextBox P4;
        private System.Windows.Forms.TextBox P3;
        private System.Windows.Forms.TextBox P1;
        private System.Windows.Forms.TextBox P2;
        private System.Windows.Forms.TextBox Amin;
        private System.Windows.Forms.TextBox Amax;
        private System.Windows.Forms.TextBox Pmin;
        private System.Windows.Forms.TextBox Pmax;
        private System.Windows.Forms.TextBox T1;
        private System.Windows.Forms.TextBox T2;
        private System.Windows.Forms.TextBox PS;
        private System.Windows.Forms.TextBox AS;
        private System.Windows.Forms.TextBox Kef6;
        private System.Windows.Forms.TextBox Kef5;
        private System.Windows.Forms.TextBox Kef4;
        private System.Windows.Forms.TextBox Kef3;
        private System.Windows.Forms.TextBox Kef2;
        private System.Windows.Forms.TextBox Kef1;
        private System.Windows.Forms.TextBox osh;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox L;
        private System.Windows.Forms.TextBox E;
        private System.Windows.Forms.TextBox HY;
        private System.Windows.Forms.TextBox HX;
        private System.Windows.Forms.TextBox Y0;
        private System.Windows.Forms.TextBox X0;
        private System.Windows.Forms.TextBox DivX;
        private System.Windows.Forms.TextBox DivY;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}