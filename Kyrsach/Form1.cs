﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChartDirector;
//using System.Windows.Forms;

namespace Kyrsach
{
	
	public partial class Form1 : Form
	{
		//
		// Array to hold all Windows.ChartViewers in the form to allow processing using loops
		//
		
		private ChartDirector.WinChartViewer[] chartViewers;
		const int N = 200;
		Model model;

		/// <summary>
		/// ChartExplorer Constructor
		/// </summary>
		public Form1()
		{
			InitializeComponent();
			//InitializeModel();
	
			chartViewers = new ChartDirector.WinChartViewer[]
			 {
				chartViewer1
			 };

		}


		private void InitializeModel()
		{
			model = new Model();
			if (model == null) return;

			model.SetA(A1, A2, A3, A4, A5,A6,Amax,Amin);
			model.SetP(P1, P2, P3, P4, P5,P6,Pmax,Pmin);
			model.SetT(T1, T2);
			model.SetSA(AS);
			model.SetSP(PS);
			model.SetER(osh);


		}
		public void createChart(WinChartViewer viewer, int chartIndex)
		{
			//-------------------------------------------------------------------------------------------------//

			// The x and y coordinates of the grid
			double[] dataX = new double[N];
			double[] dataY = new double[N];
			double[] dataZ = new double[dataX.Length * dataY.Length];
			double[] dataSA = new double[N];
			double[] dataSP = new double[N];
			//model.SetSA(AS);
			//model.SetSP(PS);
			model.Signal();
			//model.getkef();
			//model.GetX();
			//model.GetY();
			dataX = model.GetX();
			dataY = model.GetY();
			dataZ = model.GetZ();
			dataSA = model.GetSA();
			dataSP = model.GetSP();
			double amin = model.getAmin1();
			double amax = model.getAmax();
			double pmin = model.getPmin();
			double pmax = model.getPmax();
			double kef1 = model.getkef1();
			double kef2 = model.getkef2();
			double kef3 = model.getkef3();
			double kef4 = model.getkef4();
			double kef5 = model.getkef5();
			double kef6 = model.getkef6();
			Kef1.Text = String.Format("{0:0.0000000000000}", kef1);
			Kef2.Text = String.Format("{0:0.0000000000000}", kef2);
			Kef3.Text = String.Format("{0:0.0000000000000}", kef3);
			Kef4.Text = String.Format("{0:0.0000000000000}", kef4);
			Kef5.Text = String.Format("{0:0.0000000000000}", kef5);
			Kef6.Text = String.Format("{0:0.0000000000000}", kef6);

			//double[] dataA = {0};
			//double[] dataP = {0};

			//--------------------------------------------------------------------------------------------------//

			// Create a XYChart object of size 600 x 500 pixels
			XYChart c = new XYChart(600, 500);

			// Add a title to the chart using 15 points Arial Bold Italic font
			c.addTitle("Диаграмма направленности       ", "Arial Bold Italic", 15);

			// Set the plotarea at (75, 40) and of size 400 x 400 pixels. Use semi-transparent black
			// (80000000) dotted lines for both horizontal and vertical grid lines
			//c.setPlotArea(75, 40, 400, 400, -1, -1, -1, c.dashLineColor(unchecked((int)0x80000000),
			c.setPlotArea(75, 40, 400, 400, -1, -1, -1, c.dashLineColor(unchecked((int)0x80000000),
				Chart.DotLine), -1);

			// Set x-axis and y-axis title using 12 points Arial Bold Italic font
			c.xAxis().setTitle("Угол азимута, градусы.", "Arial Bold Italic", 12);
			c.yAxis().setTitle("Угол места, градусы", "Arial Bold Italic", 12);

			// Set x-axis and y-axis labels to use Arial Bold font
			c.xAxis().setLabelStyle("Arial Bold");
			c.yAxis().setLabelStyle("Arial Bold");

			// When auto-scaling, use tick spacing of 40 pixels as a guideline
			c.yAxis().setLinearScale(pmin,pmax, 1);
			c.xAxis().setLinearScale(amin, amax, 1);
			//c.yAxis().setTickDensity(40);
			//c.xAxis().setTickDensity(40);
			//c.xAxis().setAutoScale();
			//c.yAxis().setAutoScale();



			// Add a contour layer using the given data
			ContourLayer layer = c.addContourLayer(dataX, dataY, dataZ);
			
			// Move the grid lines in front of the contour layer
			c.getPlotArea().moveGridBefore(layer);

			// Add a scatter layer to the chart to show the position of the data points
			c.addScatterLayer(dataSA, dataSP, "", Chart.Cross2Shape(0.2), 20, 0xff);

			// Add a color axis (the legend) in which the top left corner is anchored at (505, 40).
			// Set the length to 400 pixels and the labels on the right side.
			ColorAxis cAxis = layer.setColorAxis(505, 40, Chart.TopLeft, 400, Chart.Right);

			//cAxis.setLinearScale(0, 1, 0.1);
			//cAxis.setAutoScale();

			//double[] colorScale = {0, 0xffffff,0.125, 0xfee08b, 0.25, 0xfdae61, 0.5, 0xf46d43, 0.75, 0xd73027, 1};
			
			double[] colorScale = {0, 0xffffff, 0.5, 0xf46d43, 0.75, 0xd73027, 1, 0xff0000 };
		    
			//double[] colorScale = { 0, 0xffffff, 0.5, 0xffffff, 0.75, 0xd73027, 1, 0xff0000 };
			
			cAxis.setColorScale(colorScale);

			// Add a title to the color axis using 12 points Arial Bold Italic font
			cAxis.setTitle("", "Arial Bold Italic", 12);
			
			// Set color axis labels to use Arial Bold font
			cAxis.setLabelStyle("Arial Bold");

			// Output the chart
			viewer.Chart = c;
			
			//------------------------------------------------------------------------------------------------------------//

		}

		

		private void Form1_Paint(object sender, EventArgs e)
		{
			
		}


		private void Create_button_Click(object sender, EventArgs e)
		{
			InitializeModel();
			createChart(chartViewers[0], 1);
			



		}

		private void button1_Click(object sender, EventArgs e)
		{
			Environment.Exit(0);
		}


		Form2 form = new Form2();
		private void newform_Click(object sender, EventArgs e)
		{
			form.Show();
		}

		
	}
}
	

