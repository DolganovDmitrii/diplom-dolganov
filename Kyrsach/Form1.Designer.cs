﻿namespace Kyrsach
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.Create_button = new System.Windows.Forms.Button();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.P6 = new System.Windows.Forms.TextBox();
            this.A6 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.P5 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.A5 = new System.Windows.Forms.TextBox();
            this.P4 = new System.Windows.Forms.TextBox();
            this.A1 = new System.Windows.Forms.TextBox();
            this.A2 = new System.Windows.Forms.TextBox();
            this.P3 = new System.Windows.Forms.TextBox();
            this.P1 = new System.Windows.Forms.TextBox();
            this.P2 = new System.Windows.Forms.TextBox();
            this.A4 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.Amax = new System.Windows.Forms.TextBox();
            this.Pmax = new System.Windows.Forms.TextBox();
            this.Amin = new System.Windows.Forms.TextBox();
            this.Pmin = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.T1 = new System.Windows.Forms.TextBox();
            this.T2 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PS = new System.Windows.Forms.TextBox();
            this.AS = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Kef6 = new System.Windows.Forms.TextBox();
            this.Kef5 = new System.Windows.Forms.TextBox();
            this.Kef4 = new System.Windows.Forms.TextBox();
            this.Kef3 = new System.Windows.Forms.TextBox();
            this.Kef2 = new System.Windows.Forms.TextBox();
            this.Kef1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.osh = new System.Windows.Forms.TextBox();
            this.newform = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Create_button
            // 
            this.Create_button.Location = new System.Drawing.Point(716, 503);
            this.Create_button.Name = "Create_button";
            this.Create_button.Size = new System.Drawing.Size(236, 43);
            this.Create_button.TabIndex = 3;
            this.Create_button.Text = "Отрисовка";
            this.Create_button.UseVisualStyleBackColor = true;
            this.Create_button.Click += new System.EventHandler(this.Create_button_Click);
            // 
            // chartViewer1
            // 
            this.chartViewer1.Location = new System.Drawing.Point(58, 22);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(316, 237);
            this.chartViewer1.TabIndex = 0;
            this.chartViewer1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.P6);
            this.groupBox1.Controls.Add(this.A6);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.P5);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.A5);
            this.groupBox1.Controls.Add(this.P4);
            this.groupBox1.Controls.Add(this.A1);
            this.groupBox1.Controls.Add(this.A2);
            this.groupBox1.Controls.Add(this.P3);
            this.groupBox1.Controls.Add(this.P1);
            this.groupBox1.Controls.Add(this.P2);
            this.groupBox1.Controls.Add(this.A4);
            this.groupBox1.Controls.Add(this.A3);
            this.groupBox1.Location = new System.Drawing.Point(716, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 206);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Азимут и место лучей антенны";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(56, 170);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "6 -";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(56, 144);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "5 -";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(56, 118);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "4 -";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(56, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "3 -";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(56, 67);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "2 -";
            // 
            // P6
            // 
            this.P6.Location = new System.Drawing.Point(153, 167);
            this.P6.Name = "P6";
            this.P6.Size = new System.Drawing.Size(66, 20);
            this.P6.TabIndex = 5;
            this.P6.Text = "0";
            // 
            // A6
            // 
            this.A6.Location = new System.Drawing.Point(81, 168);
            this.A6.Name = "A6";
            this.A6.Size = new System.Drawing.Size(66, 20);
            this.A6.TabIndex = 5;
            this.A6.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(56, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "1 -";
            // 
            // P5
            // 
            this.P5.Location = new System.Drawing.Point(153, 141);
            this.P5.Name = "P5";
            this.P5.Size = new System.Drawing.Size(66, 20);
            this.P5.TabIndex = 4;
            this.P5.Text = "0,73";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Номер луча:";
            // 
            // A5
            // 
            this.A5.Location = new System.Drawing.Point(81, 142);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(66, 20);
            this.A5.TabIndex = 4;
            this.A5.Text = "2,33";
            // 
            // P4
            // 
            this.P4.Location = new System.Drawing.Point(153, 115);
            this.P4.Name = "P4";
            this.P4.Size = new System.Drawing.Size(66, 20);
            this.P4.TabIndex = 3;
            this.P4.Text = "-1,8";
            // 
            // A1
            // 
            this.A1.Location = new System.Drawing.Point(81, 38);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(66, 20);
            this.A1.TabIndex = 0;
            this.A1.Text = "-2,33";
            // 
            // A2
            // 
            this.A2.Location = new System.Drawing.Point(81, 64);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(66, 20);
            this.A2.TabIndex = 1;
            this.A2.Text = "-1,6";
            // 
            // P3
            // 
            this.P3.Location = new System.Drawing.Point(153, 90);
            this.P3.Name = "P3";
            this.P3.Size = new System.Drawing.Size(66, 20);
            this.P3.TabIndex = 2;
            this.P3.Text = "2,4";
            // 
            // P1
            // 
            this.P1.Location = new System.Drawing.Point(153, 38);
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(66, 20);
            this.P1.TabIndex = 0;
            this.P1.Text = "0,73";
            // 
            // P2
            // 
            this.P2.Location = new System.Drawing.Point(153, 64);
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(66, 20);
            this.P2.TabIndex = 1;
            this.P2.Text = "-1,8";
            // 
            // A4
            // 
            this.A4.Location = new System.Drawing.Point(81, 116);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(66, 20);
            this.A4.TabIndex = 3;
            this.A4.Text = "1,6";
            // 
            // A3
            // 
            this.A3.Location = new System.Drawing.Point(81, 90);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(66, 20);
            this.A3.TabIndex = 2;
            this.A3.Text = "0";
            // 
            // Amax
            // 
            this.Amax.Location = new System.Drawing.Point(6, 31);
            this.Amax.Name = "Amax";
            this.Amax.Size = new System.Drawing.Size(78, 20);
            this.Amax.TabIndex = 6;
            this.Amax.Text = "5";
            this.Amax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pmax
            // 
            this.Pmax.Location = new System.Drawing.Point(6, 31);
            this.Pmax.Name = "Pmax";
            this.Pmax.Size = new System.Drawing.Size(75, 20);
            this.Pmax.TabIndex = 7;
            this.Pmax.Text = "5";
            this.Pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Amin
            // 
            this.Amin.Location = new System.Drawing.Point(6, 70);
            this.Amin.Name = "Amin";
            this.Amin.Size = new System.Drawing.Size(78, 20);
            this.Amin.TabIndex = 8;
            this.Amin.Text = "-5";
            this.Amin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pmin
            // 
            this.Pmin.Location = new System.Drawing.Point(6, 69);
            this.Pmin.Name = "Pmin";
            this.Pmin.Size = new System.Drawing.Size(75, 20);
            this.Pmin.TabIndex = 9;
            this.Pmin.Text = "-5";
            this.Pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Location = new System.Drawing.Point(716, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(236, 122);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Предельные значения";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.Pmin);
            this.groupBox5.Controls.Add(this.Pmax);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Location = new System.Drawing.Point(128, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(102, 95);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Место";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Max";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Min";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Amin);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.Amax);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(9, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(108, 96);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Азимут";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Min";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Max";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.T1);
            this.groupBox6.Controls.Add(this.T2);
            this.groupBox6.Location = new System.Drawing.Point(716, 352);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(236, 145);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Параметры расчёта";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Ср.кв. отклонение по азимуту";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(148, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ср.кв. отклонение по месту";
            // 
            // T1
            // 
            this.T1.Location = new System.Drawing.Point(69, 42);
            this.T1.Name = "T1";
            this.T1.Size = new System.Drawing.Size(103, 20);
            this.T1.TabIndex = 3;
            this.T1.Text = "1";
            this.T1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // T2
            // 
            this.T2.Location = new System.Drawing.Point(69, 106);
            this.T2.Name = "T2";
            this.T2.Size = new System.Drawing.Size(103, 20);
            this.T2.TabIndex = 2;
            this.T2.Text = "1";
            this.T2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.PS);
            this.groupBox7.Controls.Add(this.AS);
            this.groupBox7.Location = new System.Drawing.Point(958, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(225, 75);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Координаты моделируемого сигнала";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(146, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Место";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Азимут";
            // 
            // PS
            // 
            this.PS.Location = new System.Drawing.Point(116, 45);
            this.PS.Name = "PS";
            this.PS.Size = new System.Drawing.Size(103, 20);
            this.PS.TabIndex = 1;
            this.PS.Text = "0";
            this.PS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AS
            // 
            this.AS.Location = new System.Drawing.Point(6, 45);
            this.AS.Name = "AS";
            this.AS.Size = new System.Drawing.Size(103, 20);
            this.AS.TabIndex = 0;
            this.AS.Text = "0";
            this.AS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.Kef6);
            this.groupBox8.Controls.Add(this.Kef5);
            this.groupBox8.Controls.Add(this.Kef4);
            this.groupBox8.Controls.Add(this.Kef3);
            this.groupBox8.Controls.Add(this.Kef2);
            this.groupBox8.Controls.Add(this.Kef1);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Location = new System.Drawing.Point(958, 93);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(225, 253);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Показатель коэффиента усислиения";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(62, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "3 -";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(62, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "2 -";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(62, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "1 -";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(62, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "6 -";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(62, 174);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "5 -";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(62, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "4 -";
            // 
            // Kef6
            // 
            this.Kef6.Location = new System.Drawing.Point(85, 197);
            this.Kef6.Name = "Kef6";
            this.Kef6.Size = new System.Drawing.Size(100, 20);
            this.Kef6.TabIndex = 7;
            // 
            // Kef5
            // 
            this.Kef5.Location = new System.Drawing.Point(85, 171);
            this.Kef5.Name = "Kef5";
            this.Kef5.Size = new System.Drawing.Size(100, 20);
            this.Kef5.TabIndex = 6;
            // 
            // Kef4
            // 
            this.Kef4.Location = new System.Drawing.Point(85, 146);
            this.Kef4.Name = "Kef4";
            this.Kef4.Size = new System.Drawing.Size(100, 20);
            this.Kef4.TabIndex = 5;
            // 
            // Kef3
            // 
            this.Kef3.Location = new System.Drawing.Point(85, 120);
            this.Kef3.Name = "Kef3";
            this.Kef3.Size = new System.Drawing.Size(100, 20);
            this.Kef3.TabIndex = 4;
            // 
            // Kef2
            // 
            this.Kef2.Location = new System.Drawing.Point(85, 94);
            this.Kef2.Name = "Kef2";
            this.Kef2.Size = new System.Drawing.Size(100, 20);
            this.Kef2.TabIndex = 3;
            // 
            // Kef1
            // 
            this.Kef1.Location = new System.Drawing.Point(85, 68);
            this.Kef1.Name = "Kef1";
            this.Kef1.Size = new System.Drawing.Size(100, 20);
            this.Kef1.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Номер луча:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(958, 503);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 43);
            this.button1.TabIndex = 14;
            this.button1.Text = "Выход";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.osh);
            this.groupBox2.Location = new System.Drawing.Point(958, 352);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(225, 76);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры шума";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(44, 33);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 13);
            this.label23.TabIndex = 18;
            this.label23.Text = "ОСШ -";
            // 
            // osh
            // 
            this.osh.Location = new System.Drawing.Point(85, 30);
            this.osh.Name = "osh";
            this.osh.Size = new System.Drawing.Size(100, 20);
            this.osh.TabIndex = 0;
            this.osh.Text = "0,1";
            // 
            // newform
            // 
            this.newform.Location = new System.Drawing.Point(958, 434);
            this.newform.Name = "newform";
            this.newform.Size = new System.Drawing.Size(225, 63);
            this.newform.TabIndex = 16;
            this.newform.Text = "Функционал пеленгации";
            this.newform.UseVisualStyleBackColor = true;
            this.newform.Click += new System.EventHandler(this.newform_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 558);
            this.Controls.Add(this.newform);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chartViewer1);
            this.Controls.Add(this.Create_button);
            this.Name = "Form1";
            this.Text = "Курсова работа. Долганов.";
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button Create_button;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox A5;
        private System.Windows.Forms.TextBox A4;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.TextBox P5;
        private System.Windows.Forms.TextBox P4;
        private System.Windows.Forms.TextBox P3;
        private System.Windows.Forms.TextBox P2;
        private System.Windows.Forms.TextBox P1;
        private System.Windows.Forms.TextBox A6;
        private System.Windows.Forms.TextBox P6;
        private System.Windows.Forms.TextBox Amax;
        private System.Windows.Forms.TextBox Pmax;
        private System.Windows.Forms.TextBox Amin;
        private System.Windows.Forms.TextBox Pmin;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox T2;
        private System.Windows.Forms.TextBox T1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PS;
        private System.Windows.Forms.TextBox AS;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Kef6;
        private System.Windows.Forms.TextBox Kef5;
        private System.Windows.Forms.TextBox Kef4;
        private System.Windows.Forms.TextBox Kef3;
        private System.Windows.Forms.TextBox Kef2;
        private System.Windows.Forms.TextBox Kef1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox osh;
        private System.Windows.Forms.Button newform;
    }
}

